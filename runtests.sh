#!/bin/bash

cd tests/;
for file in $(ls tests*.sh); do
  echo "[INFO] Running tests from test file (${file})."
  { ./$file && echo "[PASS] Tests passed."; } || echo "[FAIL] Tests failed."
done
