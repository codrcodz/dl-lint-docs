#!/bin/bash

oneTimeSetUp() {

  # Export path so that spell wrapper is in path
  export OLDPATH="$PATH"
  export PATH="$(realpath $(dirname ../${0})):$PATH";

};

testSpellPassFailCases() {

  # Tests error handling
  assertSame "[FAIL] No parameters passed to /usr/bin/spell" "$(spell 2>&1)";
  assertSame "[FAIL] One or more unmatched words found." "$(spell mock/dirtyfile.txt 2>&1 | tail -n 1)";
  assertSame "1" "$(spell mock/dirtyfile.txt &>/dev/null; echo $?)";

  # Tests success condition
  assertSame "[PASS] No unmatched words found." "$(spell mock/cleanfile.txt)";  
  assertSame "[PASS] No unmatched words found." "$(spell -d mock/.spellrc mock/falseposfile.txt)";
  export PATH="$OLDPATH";

};

source $(which shunit2);
