A ReStructuredText Primer
=========================

Structure
---------

From the **outset**, let me say that "Structured Text" is probably a bit
of a misnomer.  It's more like "Relaxed Text" that uses certain
consistent patterns.  These patterns are interpreted by a HTML
converter to produce "Very Structured Text" that can be used by a web
browser.

The most basic pattern recognised is a **paragraph**.
That's a chunk of text that is separated by blank lines (one is
enough).  Paragraphs must have the same indentation -- that is, line
up at their left edge.  Paragraphs that start indented will result in
indented quote paragraphs. For example::

  This is a paragraph.  It's quite
  short.

     This paragraph will result in an indented block of
     text, typically used for quoting other text.

  This is another one.

Results in:

  This is a paragraph.  It's quite
  short.

     This paragraph will result in an indented block of
     text, typically used for quoting other text.

  This is another one.
