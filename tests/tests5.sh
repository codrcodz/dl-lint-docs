#!/bin/bash

if ! which hunspell;
then
   echo '[FAIL] Missing hunspell'
   exit 1
fi

if ! which pyspelling;
then
  echo '[FAIL] Missing pyspelling'
  exit 1
fi

if pyspelling --config mock/.pyspelling.yml --spellchecker hunspell;
then
  echo '[FAIL] pyspelling should have found spelling errors for this test.'
  exit 1
fi

if ! pyspelling --config mock/.pyspelling-pass.yml --spellchecker hunspell;
then
  echo '[FAIL] pyspelling should not have found spelling errors'
  exit 1
fi
